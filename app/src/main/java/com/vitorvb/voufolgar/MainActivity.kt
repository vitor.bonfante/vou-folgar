package com.vitorvb.voufolgar

import android.content.Context
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.DatePicker
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import java.time.DayOfWeek
import java.time.LocalDate
import java.util.*

class MainActivity : AppCompatActivity() {
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val sharedRef = getSharedPreferences("com.vitorvb.voufolgar.preferences.FOLGA",Context.MODE_PRIVATE) ?: return

        val date = findViewById<DatePicker>(R.id.datePicker)
        val btn = findViewById<Button>(R.id.btnCheckDay)
        val resultText = findViewById<TextView>(R.id.answer_text)
        val lastConsult = findViewById<TextView>(R.id.last_consult)

        val local = sharedRef.getString("FOLGAR",null)
        if(local != null){
            lastConsult.setText(local)
        }


        btn.setOnClickListener{
            val day = date.dayOfMonth
            val month = date.month+1
            val year = date.year

            val dateObject = LocalDate.of(year,month,day)
            val dayWeek = dateObject.dayOfWeek



            if(dayWeek == DayOfWeek.SATURDAY || dayWeek == DayOfWeek.SUNDAY){
                resultText.setText("Você irá folgar!")
                with(sharedRef.edit()){
                    putString("FOLGAR","Sua ultima consulta foi: Vai folgar sim")
                    commit()
                }
            }else{
                resultText.setText("Você não irá folgar, bora trabalhar")
                with(sharedRef.edit()){
                    putString("FOLGAR","Sua ultima consulta foi: Não vai folgar :(")
                    commit()
                }
            }
             //ovo vira o coringa
            val local = sharedRef.getString("FOLGAR",null)
            if(local != null){
                lastConsult.setText(local)
            }
        }

    }
}